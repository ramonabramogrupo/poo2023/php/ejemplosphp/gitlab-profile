![profe](phpclase.jpeg)

# Programacion en php

Hola, soy Ramon Abramo profesor de programacion en php.

## Bienvenidos a mi repositorio de PHP en GitLab

En este repositorio he colocado los ejemplos realizados en clase de la programacion en php.

Este repositorio está dedicado a proporcionar ejemplos prácticos y útiles de programación en PHP. Los ejemplos cubren una amplia gama de temas, desde la introducción a la programación hasta conceptos más avanzados como la Programación Orientada a Objetos (POO) y el paradigma del Modelo-Vista-Controlador (MVC).

## Repositorio de ejemplos

Para ir directamente a los ejemplos [pincha aqui](https://gitlab.com/ramonabramogrupo/poo2023/php/ejemplosphp/ejemplos/-/blob/master/readme.md?ref_type=heads)

## Introducción a la programación en PHP

Aquí encontrarás ejemplos básicos que te ayudarán a entender los fundamentos de la programación en PHP. Estos ejemplos son ideales para aquellos que están empezando a aprender PHP o programación en general.

![php](phpelefante.jpg)

## Programación Orientada a Objetos (POO)

La POO es un paradigma de programación que utiliza objetos y sus interacciones para diseñar aplicaciones y programas de software. En esta sección, encontrarás ejemplos que te ayudarán a entender cómo implementar la POO en PHP.

## Aplicación MVC con POO

Finalmente, te presento una aplicación completa desarrollada con el paradigma MVC utilizando la POO. Este ejemplo te mostrará cómo se puede estructurar una aplicación de manera eficiente utilizando MVC y POO.

![ramon](ramon.jpeg)